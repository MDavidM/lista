## Crear proyecto:

```
composer create-project --prefer-dist laravel/laravel lista
cp .env.example .env
php artisan key:gen
php artisan serve
```

## Crear control de usuarios

```
php artisan make:auth
```

## Preparar base de datos:

- Crear base de datos en phpmyadmin
- Ejecutar `php artisan migrate`
- Reiniciar aplicación

## Probar a registrarse y hacer login

- Traduce la plantilla "home" al español
- Ponle nombre a la aplicación en ".env"
- Hay que reiniciar el servicio ...

## Crear tablas de listas y elementos

php artisan make:migrat create_listas_table
php artisan make:migra create_elementos_lista_table


## Definir la tabla de listas:

```
Schema::create('listas', function (Blueprint $table) {
    $table->bigIncrements('id');
    $table->string('nombre');
    $table->timestamps();
});
```


## Definir la tabla de elementos:

```
Schema::create('elementos', function (Blueprint $table) {
    $table->bigIncrements('id');
    $table->string('texto');
    $table->bigInteger('lista_id')->unsigned();
    $table->foreign('lista_id')->references('id')->on('listas');
    $table->boolean('hecho');
    $table->timestamps();
});
```

## Hay que crear:

- Modelo de listas
    `php artisan make:model Lista`


## Mostrar la "lista de listas ... "

- Agregar un botón al "home"

```
<a href="/listas" class="btn btn-primary">Ver mis listas</a>
```


## Crear ruta, modelo y controlador:

```
Route::resource('/listas', 'ListaController');
```

```
php artisan make:model Lista
php artisan make:controller
```

## Comprobar que el método index ya "funciona"

```
    public function index()
    {
        return "listas";
    }
```


## Buscar la lista de _listas_

```
    public function index()
    {
        $listas = Lista::all();
        return view('lista.index', ['listas' => $listas]);
    }
```


## Añadimos un botón de "nuevo"

```
    <a class="btn btn-primary" href="/listas/create">Nueva</a>
```
